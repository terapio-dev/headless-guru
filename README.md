# terap.io-dev / headless guru

## Your journey

- clone repository
- make a brand-new component with given design `./design/component.png` and data from `./design/dataSource.js`
- display/test it in Storybook
- request an access to this project and make a merge request
- let us know you are ready to review

### Good to know

- you should ask if you are not sure
- use Typescript
- use Material-UI theme (`./src/theme`), components, icons, ...
- keep code clean and readable

## Basic usage

```shell
yarn
yarn storybook
```
