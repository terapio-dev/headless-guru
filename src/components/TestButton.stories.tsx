import TestButton, { TestButtonProps } from "./TestButton";

export default {
  title: "Components/Button",
  component: TestButton,
  argTypes: {
    onSubmit: { action: "clicked" },
  },
  args: {
    caption: "Test Button",
  },
};

const Template = (args: TestButtonProps) => <TestButton {...args} />;
export const Button = Template.bind({});
